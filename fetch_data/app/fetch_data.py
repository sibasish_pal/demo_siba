import os
import sys
import types
import pickle
import marshal
import warnings
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    train_data = pickle.load(open(f"{PICKLE_PATH}/train_data.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = fetch_data
## $xpr_param_component_type = pipeline_job 
## $xpr_param_global_variables = ["train_data"]


# To plot pretty figures

# Where to save the figures
PROJECT_ROOT_DIR = "."
CHAPTER_ID = "end_to_end_project"
IMAGES_PATH = os.path.join(PROJECT_ROOT_DIR, "images", CHAPTER_ID)
os.makedirs(IMAGES_PATH, exist_ok=True)

def save_fig(fig_id, tight_layout=True, fig_extension="png", resolution=300):
    path = os.path.join(IMAGES_PATH, fig_id + "." + fig_extension)
    print("Saving figure", fig_id)
    if tight_layout:
        plt.tight_layout()
    plt.savefig(path, format=fig_extension, dpi=resolution)

# Ignore useless warnings (see SciPy issue #5998)
warnings.filterwarnings(action="ignore", message="^internal gelsd")

# to display all the columns of the dataframe in the notebook
pd.pandas.set_option('display.max_columns', None)

# Get the data

# Data Fetching - Fetching train and test data from local drive

# Reading train_data.csv file from local drive and stored inside train_data and index set as 'id'.
train_data = pd.read_csv('train_data.csv', index_col='id')
print("Train data Shape:" ,train_data.shape)

# Reading test_data.csv file from local drive and stored inside test_data and index set as 'id'.
test_data = pd.read_csv('test_data.csv',index_col='id')
print("Test data Shape :" ,test_data.shape)

train_data.info()

# Get the count of categorical and numerical features in the training dataset.
cat = len(train_data.select_dtypes(include=['object']).columns)
num = len(train_data.select_dtypes(exclude=['object']).columns)
print('Total Features: ', cat, 'categorical', '+', num, 'numerical', '=', cat+num, 'features')

# Data Pre-processing
# 1. Remove duplicates
# 2. Split training dataset to Xfull and yfull
# 3. Find numerical columns
# 4. Find categorical columns and remove high cardinality(unique values more than 15)
#    Also, remove categorical columns with single unique values
# 5. Updating X1 after clearing out high cardinal and single unique categorical variables from Xfull.
#    (Same step has to be done to test data)
# 6. Check for missing values in X1 training dataset.
# 7. Split X1,yfull to training and validation datas
# 8. Find best approach to handle missing data with numerical columns
# 9. Find best approach to handle missing data with categorical columns(if imputed, then we have to encode it), then check RMSE
# 10.Check or strong correlations between parameters. If so, then remove one of the parameters.

# Function for removing duplicates from housing rental full training dataset
def remove_duplicates():
    train_data.drop_duplicates(inplace=True)
    
    # Find the number of rows and columns inside the training dataset
    print("Shape after dropping duplicates as Row index:" ,train_data.shape)

## Function to get a list of the variables that contain missing values by passing the dataframe
#def check_missing_values(data):
#    vars_with_na = [var for var in data.columns if data[var].isnull().sum() > 0]
#    
#    if len(vars_with_na) > 0:
#        print("\nBelow columns having missing values:")
#        print(data[vars_with_na].isnull().sum(), '\n')
#        print(data[vars_with_na].isnull().mean()*100)
#    else:
#        print("No missing values present!")
#    
#    return vars_with_na

# Function to get a list of the variables that contain missing values and plot by passing the dataframe
def missingValuesInfo(df,plot=False):
    total = df.isnull().sum().sort_values(ascending = False)
    percent = round(total/len(df)*100, 2)
    temp = pd.concat([total, percent], axis = 1,keys= ['Total', 'Percent'])
    
    if sum(total) == 0:
        print("No missing values present!")
        return temp.loc[(temp['Total'] > 0)]
    
    if plot:
        f, ax = plt.subplots(figsize=(15, 6))
        plt.xticks(rotation='90')
        sns.barplot(x=temp.index, y=temp['Percent'])
        plt.xlabel('Features', fontsize=15)
        plt.ylabel('Percent of missing values', fontsize=15)
        plt.title('Percent missing data by feature', fontsize=15)
        save_fig("missing_Values_Info")
    return temp.loc[(temp['Total'] > 0)]

# Step 1 :- Removing duplicate rows/examples from training dataset to avoid bias.
remove_duplicates()

# Check missing values in training and test dataset
#check_missing_values(train_data)
missingValuesInfo(train_data,True)

# Property type has almost 15% of rows with missing values
# Address has almost 4% of rows with missing values
# County and zipcode has less than 0.50% of rows with missing values

# Based on understanding different columns. The missing values possibly happened either due to not recorded or not available.
# county        - Not recorded
# zipcode       - Not recorded
# address       - Not recorded
# property_type - Not recorded

#check_missing_values(test_data)
missingValuesInfo(test_data,True)

# Splitting training dataset to Xfull and yfull

try:
    pickle.dump(train_data, open(f"{PICKLE_PATH}/train_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

